from tools.handle_assert import assert_result
from .conftest import pytest

from api.base_requests import BaseRequest
from tools.data_process import DataProcess


# reruns 重试次数 reruns_delay 次数之间的延时设置（单位：秒）
# 失败重跑，会影响总测试时长，如不需要 将 @pytest.mark.flaky(reruns=3, reruns_delay=5) 注释即可
@pytest.mark.flaky(reruns=2, reruns_delay=1)
def test_main(cases, get_db):     # 使用数据库功能(包含sql查询，数据备份，数据恢复)
    """

    :param cases: conftest ==> 参数化
    :param get_db: conftest ==>db
    :return:
    """
    # 此处的cases入参来自与 conftest.py  文件中 cases函数，与直接使用 @pytest.mark.parametrize
    # 有着差不多的效果
    # 发送请求
    response, expect, sql = BaseRequest.send_request(cases)
    # 执行sql
    DataProcess.handle_sql(sql, get_db)
    # 断言操作
    assert_result(response, expect)

