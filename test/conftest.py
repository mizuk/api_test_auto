import pytest
from tools.handle_db import DB
from tools.handle_file import ReadFile



@pytest.fixture(scope="session")
def get_db():
    """一次执行只实例一个db对象，节省资源"""
    try:
        db = DB()
        yield db
    finally:
        db.close()


@pytest.fixture(params=ReadFile.get_testcase())
def cases(request):
    """用例数据，测试方法参数入参该方法名 cases即可，实现同样的参数化
    目前来看相较于@pytest.mark.parametrize 更简洁。
    """
    return request.param
