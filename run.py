import os
import shutil

from config.path import *
from test.conftest import pytest
from tools.handle_common import logger




def run():
    if os.path.exists(report_path):
        shutil.rmtree(report_path)


    logger.add(log_path, enqueue=True, encoding='utf-8')

    pytest.main(args=['test/test_api.py', f'--alluredir={report_path}/data'])
    # 自动以服务形式打开报告
    # os.system(f'allure serve {report}/data')

    # 本地生成报告
    os.system(f'allure generate {report_path}/data -o {report_path}/html --clean')
    # allure  调用allure程序  .
    # generate  生成报告
    # ./ reports / data   报告的xml数据源路径
    # -o 参数名，后面跟html报告存储路径
    # ./ reports / html  参数值，指定html报告存储路径

    logger.success('报告已生成')



if __name__ == '__main__':
    run()
