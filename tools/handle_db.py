import pymysql

from tools.handle_file import ReadFile


class DB:
    mysql = ReadFile.get_config_value('$.database')

    def __init__(self):
        """初始化连接Mysql"""
        self.conn = pymysql.connect(
            host=self.mysql.get('host', 'localhost'),
            port=self.mysql.get('port', 3306),
            user=self.mysql.get('user', 'root'),
            password=self.mysql.get('password', '123456'),
            db=self.mysql.get('db_name', 'test'),
            charset=self.mysql.get('charset', 'utf8mb4'),
            cursorclass=pymysql.cursors.DictCursor
        )

    def fetch_one(self, sql: str) -> object:
        """查询数据，查一条"""
        with self.conn.cursor() as cur:
            cur.execute(sql)
            result = cur.fetchone()
            # 使用commit解决查询数据出现概率查错问题
            self.conn.commit()
        return result

    def close(self):
        """关闭数据库连接"""
        self.conn.close()


if __name__ == '__main__':
    print(ReadFile.get_config_value('$.database'))
    DB()
