from tools.handle_common import req_expr, convert_json, extractor, logger, allure_step
from tools.data_process import DataProcess



def assert_result(res: dict, expect_str: str):
    """ 预期结果实际结果断言方法
    :param res: 实际响应结果
    :param expect_str: 预期响应内容，从excel中读取
    return None
    """
    # 后置sql变量转换
    expect_str = req_expr(expect_str, DataProcess.response_dict)
    expect_dict = convert_json(expect_str)
    index = 0
    for k, v in expect_dict.items():
        # 获取需要断言的实际结果部分
        actual = extractor(res, k)
        index += 1
        logger.info(f'第{index}个断言,实际结果:{actual} | 预期结果:{v} \n断言结果 {actual == v}')
        allure_step(f'第{index}个断言', f'实际结果:{actual} = 预期结果:{v}')
        try:
            assert actual == v
        except AssertionError:
            raise AssertionError(f'第{index}个断言失败 -|- 实际结果:{actual} || 预期结果: {v}')