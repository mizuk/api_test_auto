import yaml
import xlrd
from config.path import *
from tools.handle_common import extractor


class ReadFile:
    config_dict = None

    @classmethod
    def read_config(cls, config_path: str = config_path) -> dict:
        """读取配置文件，并且转换成字典
        :param config_path: 配置文件地址， 默认使用当前项目目录下的config/config.yaml
        return cls.config_dict
        """
        if cls.config_dict is None:
            # 指定编码格式解决，win下跑代码抛出错误
            with open(config_path, 'r', encoding='utf-8') as f:
                cls.config_dict = yaml.load(f.read(), Loader=yaml.SafeLoader)
        return cls.config_dict

    @classmethod
    def get_config_value(cls, expr: str = '.') -> dict:
        """默认读取config目录下的config.yaml配置文件，根据传递的expr jsonpath表达式可任意返回任何配置项
        :param expr: 提取表达式, 使用jsonpath语法,默认值提取整个读取的对象
        return 根据表达式返回的值
        """
        return extractor(cls.read_config(), expr)


    @classmethod
    def get_testcase(cls):
        """
        读取excel格式的测试用例
        :return: data_list - pytest参数化可用的数据
        """
        data_list = []
        book = xlrd.open_workbook(testData_path)
        # 读取第一个sheet页
        table = book.sheet_by_index(0)
        # print(table)
        # print(table.nrows)
        # print(table.cell_value(1,1))
        # print(table.row_values(1))
        for row in range(1, table.nrows):
            # 每行第4列 是否运行
            if table.cell_value(row, 3) != '否':  # 每行第4列等于否将不读取内容
                value = table.row_values(row)
                value.pop(3)
                data_list.append(list(value))
        return data_list


if __name__ == '__main__':
    print(ReadFile.get_testcase())
    print(ReadFile.get_config_value('database'))
    print(ReadFile.read_config())

