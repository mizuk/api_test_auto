# api_test_auto

#### 介绍
基于python + pytest +allure + request + jsonpath + PyYaml + xlrd + loguru + pymysql 的自动化测试框架。可以通过excel直接编写测试用例，编写自定义了关联参数的写法以及通过函数生成的结果直接替换，后置sql的查询结果做断言，简化了测试用例脚本。


框架分为 api、data数据、配置文件、日志、报告、用例、工具

在excel的用例相关参数中通过& & 及 jsonpath 提取关联参数 进行替换
@   @ 结合exec使参数中调用函数的结果替换，实现用例相关数据的可扩展性

通过tools 中的方法对测试用例的数据处理，实现了现有接口可以通过excel来新增和管理接口用例。
关于用例的编写规则可以参考excel的使用说明,目前该框架已在实际应用。

