import os

# 基准地址
from datetime import datetime

base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# 配置文件地址
config_path = os.path.join(base_path, "config", "config.yaml")

# 测试数据地址
testData_path = os.path.join(base_path, "data", "case_data.xls")

# 报告地址
report_path = os.path.join(base_path, "report")

# 日志地址
time = str(datetime.now()).replace(" ", "_").split('.')[0] + "."
log_path = os.path.join(base_path, "log", time + "log")

if __name__ == '__main__':
    print(base_path)
    print(config_path)
    print(testData_path)
    print(log_path)